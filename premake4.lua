solution "PR-PROJECT"
	configurations ""
	project "project"
	kind "ConsoleApp"
	language "C++"
	files({ "src/*.h", "src/*.cpp" })
	--includedirs({"/", "includes"})

	flags({"Symbols", "ExtraWarnings"})
	links({})
	--libdirs({"Driver/"})

	buildoptions({"-std=c++14","-Wextra"})
	linkoptions({"-std=c++14"})

