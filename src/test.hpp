#ifndef __TEST_HPP__
#define __TEST_HPP__

//Standard
#include <iostream>
#include <cassert>

//Tools
#include "tools/point/Point.hpp"

#include "tools/domain/Domain.hpp"
#include "tools/domain/DomainIterator.hpp"
#include "tools/domain/NeighbourIterator.hpp"
#include "tools/domain/DomainRestrictedByFunction.hpp"

#include "tools/image/Image.hpp"
#include "tools/image/ImageWithDataFunction.hpp"
#include "tools/image/ImageRestrictedByFunction.hpp"

//Practical
#include "practical/DistanceMapAlgorithm.hpp"

namespace test
{
	using namespace concepts;

	void testGenericPoint(){
		std::cout<<std::endl<<"************************** GENERIC POINT *************************"<<std::endl<<std::endl;
		std::array<int, 4> coordsInt = {1,2,3,4};
		std::array<double, 3> coordsDouble = {1.1, 2.2, 3.3};
		std::array<bool, 2> coordsBool = {true, false};

		Point<int, 4> pInt(coordsInt);
		Point<double, 3> pDouble(coordsDouble);
		Point<bool, 2> pBool(coordsBool);
		
		std::cout<<"\t+-----------------------------------------------+"<< std::endl;
		std::cout<<"\t| 4D Point of int coords:\t" << pInt << "\t|" << std::endl;
		std::cout<<"\t| 3D Point of double coords:\t" << pDouble << "\t|"<<std::endl;
		std::cout<<"\t| 2D Point of bool coords:\t" << pBool << "\t\t|" << std::endl;
		std::cout<<"\t+-----------------------------------------------+"<< std::endl;
	}

	void testGenericBox(){
		std::cout<<std::endl<<"*************************** GENERIC BOX **************************"<<std::endl<<std::endl;
		std::array<long int,2> pMinCoords = {1, 0};
		std::array<long int,2> pMaxCoords = {9, 8};
		std::array<float,3> pMinCoordsF = {0.0, 1.2, 2.3};
		std::array<float,3> pMaxCoordsF = {3.0, 3.2, 4.3}; /* At the moment, the decimals MUST be equal to pMinCooordF's. Sorry.*/

		Point<long int,2> pMin(pMinCoords);
		Point<long int,2> pMax(pMaxCoords);

		Point<float,3> pMinF(pMinCoordsF);
		Point<float,3> pMaxF(pMaxCoordsF);

		Domain<Point<long int, 2>> domain(pMin, pMax);
		Domain<Point<float, 3>> domainF(pMinF, pMaxF);

		std::cout<<"\t+-----------------------------------------------+"<<std::endl;
		std::cout<<"\t|   2D BOX OF LONG INT\t|   3D BOX OF FLOAT     |"<<std::endl;
		std::cout<<"\t+-----------------------------------------------+"<<std::endl;
		std::cout<<"\t|   Index of pMin: "<<domain.getIndexOfPoint(pMin)<<"\t";
		std::cout<<"|   Index of pMin: "<<domainF.getIndexOfPoint(pMinF)<<"\t|"<<std::endl;
		std::cout<<"\t|   Index of pMax: "<<domain.getIndexOfPoint(pMax)<<"\t";
		std::cout<<"|   Index of pMax: "<<domainF.getIndexOfPoint(pMaxF)<<"\t|"<<std::endl;
		std::cout<<"\t|   Nb of Points:  "<<domain.getNumberOfPoints()<<"\t";
		std::cout<<"|   Nb of Points:  "<<domainF.getNumberOfPoints()<<"\t|"<<std::endl;
		std::cout<<"\t+-----------------------------------------------+"<<std::endl;
	}

	void testGenericBoxIterator(){
		std::cout<<std::endl<<"********************** GENERIC BOX ITERATOR **********************"<<std::endl<<std::endl;
		std::array<long int,2> pMinCoords = {1, 0};
		std::array<long int,2> pMaxCoords = {9, 8};
		std::array<float,3> pMinCoordsF = {0.0, 1.2, 2.3};
		std::array<float,3> pMaxCoordsF = {3.0, 3.2, 4.3}; /* At the moment, the decimals MUST be equal to pMinCooordF's. Sorry.*/

 		Point<long int,2> pMin(pMinCoords);
		Point<long int,2> pMax(pMaxCoords);

		Point<float,3> pMinF(pMinCoordsF);
		Point<float,3> pMaxF(pMaxCoordsF);

		Domain<Point<long int, 2>> domain(pMin, pMax);
		Domain<Point<float, 3>> domainF(pMinF, pMaxF);

		std::cout<<"\t\tITERATING OVER 2D BOX OF LONG INT"<<std::endl<<std::endl;
		std::cout<<domain;

		std::cout<<std::endl<<std::endl<<"\t\tITERATING OVER 3D BOX OF FLOATS"<<std::endl<<std::endl;
		std::cout<<domainF;

		std::cout<<std::endl<<std::endl<<"\t\tITERATING OVER 2D NEIGHBOURS OF LONG INT"<<std::endl<<std::endl;
		auto nbI = domain.getNeighbourIterator();
		nbI.setCenter(pMin);
		std::cout<<"\t\tCenter Point:\t"<<pMin<<std::endl<<"\t\tNeighbours:\t";
		for(auto p: nbI){
			std::cout<<p;
		}
		std::cout<<std::endl;
	}

	void testGenericImage(){
		std::cout<<std::endl<<"************************** GENERIC IMAGE *************************"<<std::endl<<std::endl;
		std::array<long int,2> pMinCoords = {1, 0};
		std::array<long int,2> pMaxCoords = {9, 8};		
 		Point<long int,2> pMin(pMinCoords);
		Point<long int,2> pMax(pMaxCoords);Domain<Point<long int, 2>> domain(pMin, pMax);

		std::cout<<"\t\t2D IMAGE OF LONG INT BUILT FROM DOMAIN"<<std::endl<<std::endl;
		Image<int, Domain<Point<long int, 2>>> image2DIntLLInt(domain);
		fillImage<Image<int, Domain<Point<long int, 2>>>>(image2DIntLLInt, 42);
		std::cout<<image2DIntLLInt;

		std::cout<<std::endl<<"\t\t3D IMAGE OF INT BUILT FROM POINTS"<<std::endl<<std::endl;
		Image<bool, Domain<Point<int, 3>>> image3DInt(
			Point<int, 3>(std::array<int, 3>({0, 0, 0})), 
			Point<int, 3>(std::array<int, 3>({2, 2, 2}))
		);
		std::cout<<image3DInt;
	}

	void testDmap(){
		std::cout<<std::endl<<"************************** DISTANCE MAP **************************"<<std::endl<<std::endl;
		std::array<long int,2> pMinCoords = {1, 0};
		std::array<long int,2> pMaxCoords = {20, 8};		
 		Point<long int,2> pMin(pMinCoords);
		Point<long int,2> pMax(pMaxCoords);Domain<Point<long int, 2>> domain(pMin, pMax);
		
		Image<int, Domain<Point<long int, 2>>> imageBool(domain);
		std::vector<int> fillVector = {
			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1
		};

		fillImage<Image<int, Domain<Point<long int, 2>>>>(imageBool, fillVector);
		std::cout<< practical::computeDMAP<Image<int, Domain<Point<long int, 2>>>>(imageBool)<<std::endl;
	}

	void testFunctionnalImage(){
		std::cout<<std::endl<<"******************** GENERIC FUNCTIONAL IMAGE ********************"<<std::endl<<std::endl;
		std::array<long int,2> pMinCoords = {1, 0};
		std::array<long int,2> pMaxCoords = {20, 8};		
 		Point<long int,2> pMin(pMinCoords);
		Point<long int,2> pMax(pMaxCoords);Domain<Point<long int, 2>> domain(pMin, pMax);
		
		Image<int, Domain<Point<long int, 2>>> imageInt(domain);
		std::vector<int> fillVectorInt = {
			2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0,
			0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1
		};

		fillImage<Image<int, Domain<Point<long int, 2>>>>(imageInt, fillVectorInt);
		auto iFun = makeImageWithDataFunction(
			imageInt,
			[](long int i) -> long int {	//ugly types
				return (i == 0 ? 0 : i*(i+1));
			}
		);

		std::cout<<iFun;
	}

	void testRestrictedImage(){
		std::cout<<std::endl<<"******************** GENERIC RESTRICTED IMAGE ********************"<<std::endl<<std::endl;
		std::array<long int,2> pMinCoords = {1, 0};
		std::array<long int,2> pMaxCoords = {20, 8};		
 		Point<long int,2> pMin(pMinCoords);
		Point<long int,2> pMax(pMaxCoords);Domain<Point<long int, 2>> domain(pMin, pMax);
		
		Image<int, Domain<Point<long int, 2>>> imageInt(domain);
		std::vector<int> fillVectorInt = {
			2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0,
			0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1
		};

		fillImage<Image<int, Domain<Point<long int, 2>>>>(imageInt, fillVectorInt);

		auto iFun = makeImageWithDataFunction(
			imageInt,
			[](long int i) -> long int {	//ugly types
				return (i == 0 ? 0 : 1);
			}
		);

		auto iRest = makeImageRestrictedByFunction(
			imageInt,
			[iFun](Point<long int, 2> p) -> long int {
				return iFun[p] == 1;
			}
		);
		std::cout<<iRest;
	}

	void testCompletePipeline(){
		std::cout<<std::endl<<"******************** COMPLETE PIPELINE ********************"<<std::endl<<std::endl;
		std::array<long int,2> pMinCoords = {1, 0};
		std::array<long int,2> pMaxCoords = {20, 8};		
 		Point<long int,2> pMin(pMinCoords);
		Point<long int,2> pMax(pMaxCoords);Domain<Point<long int, 2>> domain(pMin, pMax);
		
		Image<int, Domain<Point<long int, 2>>> imageInt(domain);
		std::vector<int> fillVectorInt = {
			2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0,
			0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0,
			0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0,
			0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1
		};

		fillImage<Image<int, Domain<Point<long int, 2>>>>(imageInt, fillVectorInt);

		auto iFun = makeImageWithDataFunction(
			imageInt,
			[](long int i) -> long int {	//ugly types
				return (i == 0 ? 0 : 1);
			}
		);

		auto iRest = makeImageRestrictedByFunction(
			imageInt,
			[iFun](Point<long int, 2> p) -> long int {
				return iFun[p] == 1;
			}
		);
		std::cout<<iRest;
	}

}

#endif // __TEST_HPP__