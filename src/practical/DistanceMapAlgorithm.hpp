#ifndef __DISTANCE_MAP_ALGORITHM_HPP__
#define __DISTANCE_MAP_ALGORITHM_HPP__

#include "../tools/image/Image.hpp"
#include <queue>

namespace practical
{
	template<typename A, typename B, typename C>
	struct GetChangedType {};

	template<typename A, typename B, typename C>
	struct GetChangedType<concepts::Image<A, B>, B, C>{
		using type = concepts::Image<C, B>;
	};

	using namespace concepts;

	template<typename V>
	typename GetChangedType<V, typename V::Domain_t, unsigned>::type computeDMAP(const V& input);


	#include "DistanceMapAlgorithm.tpp"
}

#endif // __DISTANCE_MAP_ALGORITHM_HPP__