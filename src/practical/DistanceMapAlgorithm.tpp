#ifdef __DISTANCE_MAP_ALGORITHM_HPP__

template<typename V>
typename GetChangedType<V, typename V::Domain_t, unsigned>::type computeDMAP(const V& input)
{
	typedef typename V::DomainPoint_t DomainPoint_t;

	auto domain = input.getDomain();

	const unsigned max = domain.getNumberOfPoints();
	typename GetChangedType<V, typename V::Domain_t, unsigned>::type distanceMap(domain);

	for(auto p : domain){
		distanceMap[p] = max;
	}

	std::queue<DomainPoint_t> q;
	auto n = domain.getNeighbourIterator();

	for(auto p : domain){
		if(input[p] == 1){
			distanceMap[p] = 0;
			n.setCenter(p);
			for(auto np : n){
				if(domain.hasPoint(np) && input[np] == false){
					q.push(p);
					break;
				}
			}
		}
	}

	while(! q.empty()){
		auto p = q.front();
		q.pop();
		n.setCenter(p);
		for(auto np : n){
			if(domain.hasPoint(np) && distanceMap[np] == max){
				distanceMap[np] = distanceMap[p] + 1;
				q.push(np);
			}
		}
	}

	return distanceMap;
}

#endif // __DISTANCE_MAP_ALGORITHM_HPP__