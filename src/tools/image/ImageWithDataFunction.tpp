#ifdef __IMAGE_WITH_DATA_FUNCTION_HPP__

template<typename I, typename F>
ImageWithDataFunction<I, F>::ImageWithDataFunction(const I& image, F function)
	: _image(image)
	, _function(function)
{}

template<typename I, typename F>
constexpr typename ImageWithDataFunction<I, F>::Return_t ImageWithDataFunction<I, F>::operator[](const Point_t& p) const
{
	return _function(_image[p]);
}

template<typename I, typename F>
constexpr typename ImageWithDataFunction<I, F>::Return_t ImageWithDataFunction<I, F>::operator[](const Point_t&p)
{
	return _function(_image[p]);
}

template<typename I, typename F>
const I& ImageWithDataFunction<I, F>::getImage() const
{
	return _image;
}

#endif // __IMAGE_WITH_DATA_FUNCTION_HPP__