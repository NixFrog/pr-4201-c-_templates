#ifdef __IMAGE_HPP__

template <typename T, typename D>
Image<T, D>::Image(const Domain_t& domain)
	: _domain(domain)
{
	_data.resize(_domain.getNumberOfPoints());
}

template <typename T, typename D>
Image<T, D>::Image(const std::array<DomainPointCoordinates_t, GetDimension<DomainPoint_t>::value>& maxCoordinates)
	: _domain(maxCoordinates)
{
	_data.resize(_domain.getNumberOfPoints());
}

template <typename T, typename D>
constexpr Image<T, D>::Image(const DomainPoint_t& min, const DomainPoint_t& max)
	: _domain(min, max)
{
	_data.resize(_domain.getNumberOfPoints());
}


template <typename T, typename D>
constexpr typename Image<T, D>::Data_t& Image<T, D>::operator[](const DomainPoint_t& point)
{
	size_t index;
	if(not _domain.hasPoint(point)){
		std::cerr<<"Image - accessing out of domain"<<std::endl;
		std::abort();
	}
	index = _domain.getIndexOfPoint(point);
	return _data[index];
}

template <typename T, typename D>
constexpr typename Image<T, D>::Data_t Image<T, D>::operator[](const DomainPoint_t& point) const
{
	size_t index;
	if(not _domain.hasPoint(point)){
		std::cerr<<"Image - accessing out of domain"<<std::endl;
		std::abort();
	}
	index = _domain.getIndexOfPoint(point);
	return _data[index];
}

template <typename T, typename D>
typename Image<T,D>::Domain_t Image<T, D>::getDomain() const
{
	return _domain;
}

#endif // __IMAGE_HPP__