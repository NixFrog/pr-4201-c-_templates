#ifdef __IMAGE_RESTRICTED_BY_FUNCTION_HPP__

template<typename I, typename F>
ImageRestrictedByFunction<I, F>::ImageRestrictedByFunction(const I& image, F function)
	: _image(image)
	, _function(function)
	, _domain(image.getDomain(), function)
{}

template<typename I, typename F>
constexpr typename ImageRestrictedByFunction<I, F>::Data_t ImageRestrictedByFunction<I, F>::operator[](const Point_t& p) const
{
	return _image[p];
}

template<typename I, typename F>
constexpr typename ImageRestrictedByFunction<I, F>::Data_t ImageRestrictedByFunction<I, F>::operator[](const Point_t&p)
{
	return _image[p];
}

template<typename I, typename F>
const I& ImageRestrictedByFunction<I, F>::getImage() const
{
	return _image;
}

#endif // __IMAGE_RESTRICTED_BY_FUNCTION_HPP__