#ifndef __IMAGE_WITH_DATA_FUNCTION_HPP__
#define __IMAGE_WITH_DATA_FUNCTION_HPP__

namespace concepts
{
	template<typename I, typename F>
	class ImageWithDataFunction
	{
	private:
		I _image;
		F _function;
	
	public:
		using Image_t    = I;
		using Function_t = F;
		using Data_t     = typename Image_t::Data_t;
		using Point_t    = typename Image_t::DomainPoint_t;
		using Return_t   = decltype( _function(std::declval<Data_t>()) );

		ImageWithDataFunction(const I& image, F function);
		constexpr inline Return_t operator[](const Point_t& p) const;
		constexpr inline Return_t operator[](const Point_t&p);
		const Image_t& getImage() const;
	};

	#include "ImageWithDataFunction.tpp"

	template<typename I, typename F>
	ImageWithDataFunction<I, F> makeImageWithDataFunction(const I& image, F function)
	{
		return ImageWithDataFunction<I, F>(image, function);
	}

	template<typename I, typename F>
	inline std::ostream& operator<<(std::ostream& os, const ImageWithDataFunction<I, F>& imageWDF)
	{
		auto image = imageWDF.getImage();
		os<<"\t";
		for(auto point : image.getDomain()){
			os<<imageWDF[point]<<" ";
			if(isEndOfDirection<typename ImageWithDataFunction<I, F>::Point_t>(point, image.getDomain())){
				os<<std::endl<<"\t";
			}			
		}
		os<<imageWDF[*(image.getDomain().end())];
		os<<std::endl;
		return os;
	}
}

#endif // __IMAGE_WITH_DATA_FUNCTION_HPP__