#ifndef __IMAGE_RESTRICTED_BY_FUNCTION_HPP__
#define __IMAGE_RESTRICTED_BY_FUNCTION_HPP__

#include <utility>

namespace concepts
{
	template<typename I, typename F>
	class ImageRestrictedByFunction
	{
	public:
		using Image_t       = I;
		using Function_t    = F;
		using Data_t        = typename Image_t::Data_t;
		using Point_t       = typename Image_t::DomainPoint_t;
		using ImageDomain_t = DomainRestrictedByFunction<typename Image_t::Domain_t, Function_t>;

		ImageRestrictedByFunction(const I& image, F function);
		constexpr inline Data_t operator[](const Point_t& p) const;
		constexpr inline Data_t operator[](const Point_t&p);
		const Image_t& getImage() const;
		const ImageDomain_t getDomain() const{return _domain;}

	private:
		Image_t _image;
		Function_t _function;
		ImageDomain_t _domain;
	};

	#include "ImageRestrictedByFunction.tpp"

	template<typename I, typename F>
	ImageRestrictedByFunction<I, F> makeImageRestrictedByFunction(const I& image, F function)
	{
		return ImageRestrictedByFunction<I, F>(image, function);
	}

	template<typename I, typename F>
	inline std::ostream& operator<<(std::ostream& os, const ImageRestrictedByFunction<I, F>& imageRF)
	{
		auto image = imageRF.getImage();
		os<<"\t";
		for(auto point : image.getDomain()){
			if(imageRF.getDomain().hasPoint(point)){
				os<<imageRF[point]<<" ";
			}
			else{
				os<<"  ";
			}

			if(isEndOfDirection<typename ImageRestrictedByFunction<I, F>::Point_t>(point, image.getDomain())){
				os<<std::endl<<"\t";
			}			
		}
		os<<std::endl;
		return os;
	}
}
#endif // __IMAGE_RESTRICTED_BY_FUNCTION_HPP__