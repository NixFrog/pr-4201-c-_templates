#ifndef __IMAGE_HPP__
#define __IMAGE_HPP__

#include <vector>

namespace concepts
{
	template <typename T, typename D>
	class Image
	{
	public:
		using Data_t                   = T;
		using Domain_t                 = D;
		using DomainPoint_t            = typename Domain_t::Point_t;
		using DomainDomainIterator_t   = typename Domain_t::DomainIterator_t;
		using DomainPointCoordinates_t = typename Domain_t::PointCoordinates_t;

		explicit Image(const Domain_t& domain);
		Image(const std::array<DomainPointCoordinates_t, GetDimension<DomainPoint_t>::value>& maxCoordinates);
		constexpr Image(const DomainPoint_t& min, const DomainPoint_t& max);

		constexpr inline Data_t& operator[](const DomainPoint_t& point);
		constexpr inline Data_t  operator[](const DomainPoint_t& point) const;

		Domain_t getDomain() const;

	private:
		std::vector<Data_t> _data;
		Domain_t _domain;

	// Friends
		template<typename U, typename F>
		friend std::ostream& operator<<(std::ostream& os, const Image<U, F>& image);

		template<typename I>
		friend void fillImage(I& image, typename I::Data_t v);

		template<typename I>
		friend void fillImage(I& image, std::vector<typename I::Data_t> data);
	};

	#include "Image.tpp"

	template<typename T, typename D>
	inline std::ostream& operator<<(std::ostream& os, const Image<T, D>& image)
	{
		os<<"\t";
		for(typename Image<T, D>::DomainPoint_t point : image._domain){
			os<<image[point]<<" ";
			if(isEndOfDirection<typename Image<T,D>::DomainPoint_t>(point, image._domain)){
				os<<std::endl<<"\t";
			}			
		}
		os<<image[*(image._domain.end())];
		os<<std::endl;
		return os;
	}

	template<typename I>
	void fillImage(I& image, typename I::Data_t v)
	{
		for(typename I::DomainPoint_t p : image._domain){
			image[p] = v;
		}
		image[*(image._domain.end())] = v;
	}

	template<typename I>
	void fillImage(I& image, std::vector<typename I::Data_t> data)
	{
		if(data.size() != image._domain.getNumberOfPoints()){
			std::cerr<<"Error - incorrect number of points"<<std::endl;
			std::abort();
		}

		for(typename I::DomainPoint_t p : image._domain){
			image[p] = data[image._domain.getIndexOfPoint(p)];
		}
	}
}

#endif // __IMAGE_HPP__
