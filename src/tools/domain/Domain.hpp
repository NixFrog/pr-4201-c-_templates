#ifndef __DOMAIN_HPP__
#define __DOMAIN_HPP__

#include <climits>

#include "DomainIterator.hpp"
#include "NeighbourIterator.hpp"

namespace concepts
{
	template<typename T>
	struct GetDimension : public std::integral_constant<size_t, 0> {};

	template<typename T, size_t Dimension>
	struct GetDimension<Point<T, Dimension>> : public std::integral_constant<size_t, Dimension> {};

	template<typename T>
	class Domain
	{
	public:
		using Point_t             = T;
		using GetDimension_t      = GetDimension<Point_t>;
		using DomainIterator_t    = DomainIterator<Domain<Point_t>>;
		using PointCoordinates_t  = typename Point_t::Coordinates_t;
		using NeighbourIterator_t = NeighbourIterator<Domain<Point_t>>;

		constexpr explicit Domain(std::array<PointCoordinates_t, GetDimension<Point_t>::value> maxCoordinates);
		constexpr Domain(Point_t min, Point_t max);

		constexpr bool hasPoint(const Point_t point) const;
		
		constexpr size_t getIndexOfPoint(const Point_t point) const;
		size_t getNumberOfPoints() const;

		inline Point_t getMax() const;
		inline Point_t getMin() const;

		DomainIterator_t begin() const;
		DomainIterator_t end() const;

		bool operator==(const Domain<Point_t>& domain) const;

		NeighbourIterator_t getNeighbourIterator() const;

	private:
		Point_t _min;
		Point_t _max;

	// Friends
		template<typename J>
		friend std::ostream& operator<<(std::ostream& os, const Domain<J>& domain);

		template<typename U>
		friend bool isEndOfDirection(const U& point, const Domain<U>& domain);
	};

	#include "Domain.tpp"

	template<typename T>
	inline bool isEndOfDirection(const T& point, const Domain<T>& domain)
	{
		bool isEndOfDirection = true;
		for(size_t i=0; i<Domain<T>::GetDimension_t::value - 1; i++){
			if(point[i] != domain._max[i] && isEndOfDirection == true){
				isEndOfDirection = false;
				break;
			}
			else{
				break;
			}
		}
		return isEndOfDirection;
	}

	template<typename Point_t>
	inline std::ostream& operator<<(std::ostream& os, const Domain<Point_t>& domain)
	{
		os<<"\t";
		for(Point_t point: domain){
			os<<point;
			if(isEndOfDirection<Point_t>(point, domain)){
				os<<std::endl<<"\t";
			}
		}
		os<<domain._max;
		os<<std::endl;
		return os;
	}
	
} //concepts

#endif // __DOMAIN_HPP__
