#ifndef __NEIGHBOUR_ITERATOR_HPP__
#define __NEIGHBOUR_ITERATOR_HPP__

#include <array>
#include <vector>

namespace concepts
{
	template<typename D>
	class NeighbourIterator
	{
	public:
		using Domain_t                 = D;
		using DomainPoint_t            = typename Domain_t::Point_t;
		using DomainGetDimension_t     = typename Domain_t::GetDimension_t;
		using DomainPointCoordinates_t = typename Domain_t::PointCoordinates_t;

		constexpr explicit NeighbourIterator(const DomainPoint_t& center);

		typename std::vector<DomainPoint_t>::iterator begin();
		typename std::vector<DomainPoint_t>::iterator end();

		void setCenter(const DomainPoint_t& center);

	private:
		inline void updateNeighbourList();

		DomainPoint_t _center;
		size_t _currentNeighbourIndex;
		std::vector<DomainPoint_t> _neighbours;
	};

	#include "NeighbourIterator.tpp"
}

#endif // __NEIGHBOUR_ITERATOR_HPP__