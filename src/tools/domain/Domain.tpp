#ifdef __DOMAIN_HPP__

template<typename Point_t>
constexpr Domain<Point_t>::Domain(std::array<PointCoordinates_t, GetDimension<Point_t>::value> maxCoordinates)
	: _min()
	, _max(maxCoordinates)
{
	#ifndef NDEBUG
		for(PointCoordinates_t pc : maxCoordinates){
			assert(pc != 0);
		}
	#endif //NDEBUG
}

template<typename Point_t>
constexpr Domain<Point_t>::Domain(Point_t min, Point_t max)
	: _min(min)
	, _max(max)
{}

template<typename Point_t>
constexpr bool Domain<Point_t>::hasPoint(const Point_t point) const
{
	return(point >= _min && point <= _max);
}

template<typename Point_t>
constexpr size_t Domain<Point_t>::getIndexOfPoint(const Point_t point) const
{
	assert(point<=_max && point >= _min);
	assert(GetDimension<Point_t>::value < LLONG_MAX);	// I'm a lazy bum who can't work with size_t
	
	int sum = 0;

	for(long long int i=0; i < static_cast<long long int>(GetDimension<Point_t>::value); i++){
		int mult = 1;
		for(long long int j=0; j<i; j++){
			mult *= (_max[j] - _min[j] + 1);
		}
		sum += (point[i]-_min[i]) * mult;
	}
	return sum;
}

template<typename Point_t>
size_t Domain<Point_t>::getNumberOfPoints() const
{
	size_t numberOfPoints = 1;
	for(size_t i=0; i<GetDimension<Point_t>::value; i++){
		numberOfPoints *= (_max[i] - _min[i] + 1);
	}
	return numberOfPoints;
}

template<typename Point_t>
Point_t Domain<Point_t>::getMax() const
{
	return _max;
}

template<typename Point_t>
Point_t Domain<Point_t>::getMin() const
{
	return _min;
}

template<typename Point_t>
typename Domain<Point_t>::DomainIterator_t Domain<Point_t>::begin() const
{
	DomainIterator<Domain<Point_t>> iterator(*this);
	iterator.first();
	return iterator;
}

template<typename Point_t>
typename Domain<Point_t>::DomainIterator_t Domain<Point_t>::end() const
{
	DomainIterator<Domain<Point_t>> iterator(*this, _max);
	return iterator;
}

template<typename Point_t>
bool Domain<Point_t>::operator==(const Domain<Point_t>& domain) const
{
	return(_min == domain._min && _max == domain._max);
}

template<typename Point_t>
NeighbourIterator<Domain<Point_t>> Domain<Point_t>::getNeighbourIterator() const{
	NeighbourIterator<Domain<Point_t>> neighbourIterator(_min);
	return neighbourIterator;
}


#endif // __DOMAIN_HPP__