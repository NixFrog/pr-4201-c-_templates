#ifdef __DOMAIN_ITERATOR_HPP__

template<typename Domain>
constexpr DomainIterator<Domain>::DomainIterator(const Domain& domain)
	: _domain(domain)
{}

template<typename Domain>
constexpr DomainIterator<Domain>::DomainIterator(const Domain& domain, const DomainPoint_t& point)
	: _domain(domain)
	, _currentElement(point)
{}

template<typename Domain>
void DomainIterator<Domain>::first()
{
	_currentElement = _domain.getMin();
}

template<typename Domain>
bool DomainIterator<Domain>::isDone() const
{
	return(_domain.getIndexOfPoint(_currentElement) == _domain.getNumberOfPoints() - 1);
}

template<typename Domain>
DomainIterator<Domain>& DomainIterator<Domain>::operator++()
{
	if(isDone()){
		abort();
	}

	_currentElement[0]++;
	for(size_t i=1; i < DomainGetDimension_t::value; i++){
		if(_currentElement[i-1] > _domain.getMax()[i-1]){
			_currentElement[i-1] = _domain.getMin()[i-1];
			_currentElement[i]++;
		}
	}

	return (*this);
}

template<typename Domain>
DomainIterator<Domain> DomainIterator<Domain>::operator++(int)
{
	DomainIterator<Domain> tmp(*this);
	operator++();
	return tmp;
}

template<typename Domain>
bool DomainIterator<Domain>::operator==(const DomainIterator<Domain>& domainIterator)
{
	return (_domain == domainIterator._domain && _currentElement == domainIterator._currentElement);
}

template<typename Domain>
bool DomainIterator<Domain>::operator!=(const DomainIterator<Domain>& domainIterator)
{
	return !(*this == domainIterator);
}

template<typename Domain>
typename DomainIterator<Domain>::DomainPoint_t DomainIterator<Domain>::operator*()
{
	return _currentElement;
}

template<typename Domain>
const typename DomainIterator<Domain>::DomainPoint_t DomainIterator<Domain>::operator*() const
{
	return _currentElement;
}


#endif // __DOMAIN_ITERATOR_HPP__