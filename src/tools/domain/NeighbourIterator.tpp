#ifdef __NEIGHBOUR_ITERATOR_HPP__

template<typename T>
constexpr NeighbourIterator<T>::NeighbourIterator(const DomainPoint_t& center)
	: _center(center)
{
	updateNeighbourList();
}

template<typename T>
typename std::vector<typename T::Point_t>::iterator NeighbourIterator<T>::begin()
{
	return _neighbours.begin();
}

template<typename T>
typename std::vector<typename T::Point_t>::iterator NeighbourIterator<T>::end()
{
	return _neighbours.end();
}

template<typename T>
void NeighbourIterator<T>::setCenter(const DomainPoint_t& center){
	_center = center;
	updateNeighbourList();
}

template<typename T>
void NeighbourIterator<T>::updateNeighbourList(){
	_neighbours.clear();
	std::array<DomainPointCoordinates_t, DomainGetDimension_t::value> coordinates = _center.getCoordinates();

	for(size_t i=0; i<DomainGetDimension_t::value; i++){
		coordinates[i] --;
		_neighbours.push_back(DomainPoint_t(coordinates));
		coordinates[i] ++;
		coordinates[i] ++;
		_neighbours.push_back(DomainPoint_t(coordinates));
		coordinates[i] --;
	}
}

#endif // __NEIGHBOUR_ITERATOR_HPP__