#ifndef __DOMAIN_ITERATOR_HPP__
#define __DOMAIN_ITERATOR_HPP__

namespace concepts
{
	template<typename D>
	class DomainIterator
	{
	public:
		using  Domain               = D;
		using  DomainPoint_t        = typename Domain::Point_t;
		using  DomainGetDimension_t = typename Domain::GetDimension_t;

		constexpr explicit DomainIterator(const Domain& domain);
		constexpr DomainIterator(const Domain& domain, const DomainPoint_t& point);

		void first();
		bool isDone() const;

		inline DomainIterator<Domain>& operator++();
		inline DomainIterator<Domain> operator++(int);
		inline bool operator==(const DomainIterator<Domain>& domainIterator);
		inline bool operator!=(const DomainIterator<Domain>& domainIterator);
		inline DomainPoint_t operator*();
		inline const DomainPoint_t operator*() const;

	private:
		Domain _domain;
		DomainPoint_t _currentElement;
	};

	#include "DomainIterator.tpp"

} // concepts

#endif // __DOMAIN_ITERATOR_HPP__