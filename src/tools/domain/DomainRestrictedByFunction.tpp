#ifdef __DOMAIN_RESTRICTED_BY_FUNCTION_HPP__

template<typename D, typename F>
DomainRestrictedByFunction<D, F>::DomainRestrictedByFunction(const D& domain, F function)
	: _domain(domain)
	, _function(function)
{}

template<typename D, typename F>
constexpr bool DomainRestrictedByFunction<D, F>::hasPoint(const DomainPoint_t& point) const
{
	return _function(point);
}

template<typename D, typename F>
typename D::DomainIterator_t DomainRestrictedByFunction<D, F>::begin() const
{
	return _domain.begin();
}

template<typename D, typename F>
typename D::DomainIterator_t DomainRestrictedByFunction<D, F>::end() const
{
	return _domain.end();
}

template<typename D, typename F>
typename D::NeighbourIterator_t DomainRestrictedByFunction<D, F>::getNeighbourIterator() const
{
	return _domain.getNeighbourIterator();
}

template<typename D, typename F>
size_t DomainRestrictedByFunction<D, F>::getNumberOfPoints() const
{
	return _domain.getNumberOfPoints();
}

template<typename D, typename F>
const D& DomainRestrictedByFunction<D, F>::getDomain() const
{
	return _domain;
}

#endif // __DOMAIN_RESTRICTED_BY_FUNCTION_HPP__