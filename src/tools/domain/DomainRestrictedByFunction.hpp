#ifndef __DOMAIN_RESTRICTED_BY_FUNCTION_HPP__
#define __DOMAIN_RESTRICTED_BY_FUNCTION_HPP__

#include <utility>

namespace concepts
{
	template<typename D, typename F>
	class DomainRestrictedByFunction
	{
	public:
		using Domain_t            = D;
		using Function_t          = F;
		using DomainPoint_t       = typename Domain_t::Point_t;
		using DomainIterator_t    = typename Domain_t::DomainIterator_t;
		using NeighbourIterator_t = typename Domain_t::NeighbourIterator_t;

		DomainRestrictedByFunction(const D& domain, F function);

		constexpr inline bool hasPoint(const DomainPoint_t& point) const;

		DomainIterator_t begin() const;
		DomainIterator_t end() const;
		NeighbourIterator_t getNeighbourIterator() const;

		size_t getNumberOfPoints() const;
		const Domain_t& getDomain() const;

	private:
		Domain_t _domain;
		Function_t _function;
	};

	#include "DomainRestrictedByFunction.tpp"

	template<typename D, typename F>
	DomainRestrictedByFunction<D, F> makeDomainRestrictedByFunction(const D& domain, F function)
	{
		return DomainRestrictedByFunction<D, F>(domain, function);
	}
}

#endif // __IMAGE_RESTRICTED_BY_FUNCTION_HPP__