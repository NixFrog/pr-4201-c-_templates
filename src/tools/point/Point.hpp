#ifndef __POINT_HPP__
#define __POINT_HPP__

#include <array>

namespace concepts
{
	/*
	* T must accept operator++ and operator--
	*/
	template<typename T, size_t Dimension>
	class Point
	{
	public:
		using Coordinates_t = T;

		constexpr Point();

		template<size_t ParameterDimension>
		constexpr explicit Point(std::array<Coordinates_t, ParameterDimension> coordinates);

		inline bool operator==(const Point<Coordinates_t, Dimension>& p) const;
		inline bool operator!=(const Point<Coordinates_t, Dimension>& p) const;
		inline bool operator>=(const Point<Coordinates_t, Dimension>& p) const;
		inline bool operator<=(const Point<Coordinates_t, Dimension>& p) const;
		inline Coordinates_t& operator[](const size_t& pointCoordinates);
		inline Coordinates_t  operator[](const size_t& pointCoordinates) const;

		inline std::array<Coordinates_t, Dimension> getCoordinates() const;

	private:
		std::array<Coordinates_t, Dimension> _coordinates;
	};

	#include "Point.tpp"

	template<typename Coordinates_t, size_t Dimension>
	inline std::ostream& operator<<(std::ostream& os, const Point<Coordinates_t, Dimension>& point){
		os << '(';
		for(size_t i = 0; i<Dimension-1; i++){
			os << point[i] << ", ";
		}
		os << point[Dimension-1] << ')';

		return os;
	}

} //concepts

#endif //__POINT_HPP__