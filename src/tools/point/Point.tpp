#ifdef __POINT_HPP__

template<typename Coordinates_t, size_t Dimension>
constexpr Point<Coordinates_t, Dimension>::Point(){
	_coordinates.fill(Coordinates_t());
}

template<typename Coordinates_t, size_t Dimension>
template<size_t ParameterDimension>
constexpr Point<Coordinates_t, Dimension>::Point(std::array<Coordinates_t, ParameterDimension> coordinates)
{
	assert(coordinates.size() == _coordinates.size());
	_coordinates = coordinates;
}

template<typename Coordinates_t, size_t Dimension>
bool Point<Coordinates_t, Dimension>::operator==(const Point<Coordinates_t, Dimension>& p) const
{
	return(_coordinates == p._coordinates);
}

template<typename Coordinates_t, size_t Dimension>
bool Point<Coordinates_t, Dimension>::operator!=(const Point<Coordinates_t, Dimension>& p) const
{
	return !(*(this) == p);
}

template<typename Coordinates_t, size_t Dimension>
bool Point<Coordinates_t, Dimension>::operator>=(const Point<Coordinates_t, Dimension>& p) const
{
	for(size_t i = 0; i<_coordinates.size(); i++){
		if(_coordinates[i] < p._coordinates[i]){
			return false;
		}
	}
	return true;
}

template<typename Coordinates_t, size_t Dimension>
bool Point<Coordinates_t, Dimension>::operator<=(const Point<Coordinates_t, Dimension>& p) const
{
	for(size_t i = 0; i<_coordinates.size(); i++){
		if(_coordinates[i] >  p._coordinates[i]){
			return false;
		}
	}
	return true;
}

template<typename Coordinates_t, size_t Dimension>
Coordinates_t& Point<Coordinates_t, Dimension>::operator[](const size_t& pointCoordinates)
{
	assert(_coordinates.size() > pointCoordinates);
	return _coordinates[pointCoordinates];
}
		
template<typename Coordinates_t, size_t Dimension>
Coordinates_t  Point<Coordinates_t, Dimension>::operator[](const size_t& pointCoordinates) const
{
	assert(_coordinates.size() > pointCoordinates);
	return _coordinates[pointCoordinates];
}

template<typename Coordinates_t, size_t Dimension>
std::array<Coordinates_t, Dimension> Point<Coordinates_t, Dimension>::getCoordinates() const
{
	return _coordinates;
}

#endif // __POINT_HPP__