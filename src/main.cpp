#include "test.hpp"

int main()
{
	using namespace test;

	testGenericPoint();
	testGenericBox();
	testGenericBoxIterator();
	testGenericImage();
	testDmap();
	testFunctionnalImage();
	testRestrictedImage();
	testCompletePipeline();

	exit(EXIT_SUCCESS);
}
