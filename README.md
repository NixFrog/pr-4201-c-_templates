# Modern C++ templates training
This school project contains a library for generic images of any dimension and any data type, allowing the user to run the same algorithms on graphs or rgb images.

This is demonstrated by using the library for a generic map distance on domain-limited images.

##Building
This project includes a makefile generated with premake4. In case it fails to run, try running the following command in the project's root directory:

```
premake4 clean && premake4 gmake && make
```